
import os
import sys
import numpy as np
import glob
import SimpleITK as sitk
from Model import unet2d
from Model import config
import Augmentor 

def get_training_datasets(data_folder):
    imgs = list()
    masks = list()
    subjects = list()
    for subject in glob.glob(data_folder + "/*"):
        subjects.append(os.path.basename(subject))
        img = sitk.ReadImage(subject+ config["filename_image"])
        mask = sitk.ReadImage(subject + config["filename_mask"])
        np_img = np.expand_dims(sitk.GetArrayFromImage(img), axis=0)
        np_mask = np.expand_dims(sitk.GetArrayFromImage(mask), axis=0)
        imgs.append(np_img)
        masks.append(np_mask)
    return np.array(imgs), np.array(masks), subjects

data_folder = config["data_folder"]
imgs, segs, subjects = get_training_datasets(data_folder)

# Validation split

ids = np.random.permutation(len(segs))
n_train = int(len(segs)*config["validation_split_ratio"])
training_imgs = imgs[ids[:n_train]]
training_segs = segs[ids[:n_train]]

np.save(config["training_files"][0], training_imgs)  
np.save(config["training_files"][1], training_segs)  
with open(config["training_files"][2], 'w') as f:
    for item in ids[:n_train]:
        f.write("%s\n" % subjects[item])

testing_imgs = imgs[ids[n_train:]]
testing_segs = segs[ids[n_train:]]
np.save(config["validation_files"][0], testing_imgs)  
np.save(config["validation_files"][1], testing_segs)  
with open(config["validation_files"][2], 'w') as f:
    for item in ids[n_train:]:
        f.write("%s\n" % subjects[item])

# Model

model = unet2d(input_shape=config["input_shape"], n_labels=config["n_labels"],
                                  initial_learning_rate=config["initial_learning_rate"],
                                  n_base_filters=config["n_base_filters"])

# Generator

gen = Augmentor.image_augmentation(training_imgs,training_segs).aug_train_iterator()


'''
img, seg = next(gen)

import matplotlib.pyplot as plt

plt.imshow(img[0,0,:,:])
plt.show()
plt.imshow(seg[0,0,:,:])
plt.show()
'''


# Training

from keras.callbacks import ModelCheckpoint, CSVLogger, LearningRateScheduler, ReduceLROnPlateau, EarlyStopping

def get_callbacks(model_file, initial_learning_rate=0.0001, learning_rate_drop=0.5, learning_rate_epochs=None,
                  learning_rate_patience=50, logging_file="training.log", verbosity=1,
                  early_stopping_patience=None):
    callbacks = list()
    callbacks.append(ModelCheckpoint(model_file, save_best_only=True))
    callbacks.append(CSVLogger(logging_file, append=True))
    if learning_rate_epochs:
        callbacks.append(LearningRateScheduler(partial(step_decay, initial_lrate=initial_learning_rate,
                                                       drop=learning_rate_drop, epochs_drop=learning_rate_epochs)))
    else:
        callbacks.append(ReduceLROnPlateau(factor=learning_rate_drop, patience=learning_rate_patience,
                                           verbose=verbosity))
    if early_stopping_patience:
        callbacks.append(EarlyStopping(verbose=verbosity, patience=early_stopping_patience))
    return callbacks

model_file = config["model_file"]
model.fit_generator(generator=gen, epochs=config["n_epochs"], verbose=1, steps_per_epoch=config["steps_per_epoch"],
                        validation_data=(testing_imgs,testing_segs),
                        callbacks=get_callbacks(model_file,
                                                initial_learning_rate=config["initial_learning_rate"] ,
                                                learning_rate_drop=config["learning_rate_drop"],
                                                learning_rate_epochs=None,
                                                learning_rate_patience=config["patience"],
                                                early_stopping_patience=config["early_stop"]))

