# Implementation of 2D U-Net with Keras

This implementation builds a U-Net in 2D based on the model proposed by Isensee et al. in 2017: https://arxiv.org/abs/1802.10508

Original implementation: https://github.com/ellisdg/3DUnetCNN

## Getting Started

Save datasets in folder: */train_set*

Create for each subject a subfolder with image and ground trush mask.

```
e.g. ./train_set/subject1/img.nii.gz, ./train_set/subject1/mask.nii.gz
```

### Prerequisites

Tensorflow, Keras, Numpy, SimpleItk 

### Model

Check Configurations: inputs, image shape, validation split, model options, fit generator options, filenames.

The visualisation of net will be ploted to *model.png*.

## Running

Run the training with

```
python Run.py
```

Datasets will be saved into *.npy* after validation split.

And then

the model will be saved as *Net.hdf5* after each epoch,

training log of loss and val_loss is avaliable in *training.log*.

### Calculate Dice Score for Validation Datasets


```
python Evaluate.py
```

This step saves prediction results for validation datasets in *./prediction*

Also, average Dice Score (2*intersection/(gt+pred)) will appear in terminal.

## Authors

Original implementation: https://github.com/ellisdg/3DUnetCNN

Zihao Wang, Antoine Despinasse participated in this project.

