
import numpy as np
import os
import SimpleITK as sitk
from keras.models import load_model

from Model import config, dice_coefficient, dice_coefficient_loss

testing_imgs = np.load(config["validation_files"][0])
testing_segs = np.load(config["validation_files"][1])
testing_subjects = list()
with open(config["validation_files"][2], 'r') as f:  
    for line in f:
        sub = line[:-1]
        testing_subjects.append(sub)

def load_old_model(model_file):
    print("Loading pre-trained model")
    custom_objects = {'dice_coefficient_loss': dice_coefficient_loss, 'dice_coefficient': dice_coefficient}
    try:
        from keras_contrib.layers import InstanceNormalization
        custom_objects["InstanceNormalization"] = InstanceNormalization
    except ImportError:
        pass
    try:
        return load_model(model_file, custom_objects=custom_objects)
    except ValueError as error:
        if 'InstanceNormalization' in str(error):
            raise ValueError(str(error) + "\n\nPlease install keras-contrib to use InstanceNormalization:\n"
                                          "'pip install git+https://www.github.com/keras-team/keras-contrib.git'")
        else:
            raise error

model = load_old_model(config["model_file"])

predictions = model.predict(testing_imgs, batch_size=1, verbose=1)

def get_mask(data):
    return np.array(data) >= 0.5

def write_results(folder, overwrite=False):
    if not os.path.exists(folder) or overwrite:
        if not os.path.exists(folder):
            os.makedirs(folder)
    dice = 0 
    for i in range(len(testing_segs)):
        gt = testing_segs[i]
        img = testing_imgs[i]
        pred = get_mask(predictions[i])
        dice = dice + (2*np.sum(gt * pred))/(np.sum(gt)+np.sum(pred))
        sub = testing_subjects[i]
        subject_folder = os.path.join(folder, sub)
        if not os.path.exists(subject_folder) or overwrite:
            if not os.path.exists(subject_folder):
                os.makedirs(subject_folder)
            sitk.WriteImage(sitk.GetImageFromArray(img), subject_folder + config["output_filenames"][0], True)
            sitk.WriteImage(sitk.GetImageFromArray(gt), subject_folder + config["output_filenames"][1], True)
            sitk.WriteImage(sitk.GetImageFromArray(pred.astype(int)), subject_folder + config["output_filenames"][2], True)
    print("dice = " + str(dice/len(testing_segs)))

write_results(config["folder_prediction"])

'''
import matplotlib.pyplot as plt
def plot(img, gt, pred):
    plt.imshow(img[0,:,:])
    plt.show()
    plt.imshow(gt[0,:,:])
    plt.show()
    plt.imshow(pred[0,:,:])
    plt.show()

'''

